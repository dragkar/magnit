import db.DAO.EntityDao;
import db.DAO.EntityDaoImpl;
import db.service.DaoService;
import db.service.XmlCreatorFromDb;
import db.service.XmlCreatorFromFile;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        long start =System.currentTimeMillis();
        DaoService daoService = new DaoService();
        daoService.inertElements(1000000);

        XmlCreatorFromDb xmlCreatorFromDb = new XmlCreatorFromDb();
        xmlCreatorFromDb.createXmlFile("file_1.xml");
        XmlCreatorFromFile xmlCreatorFromFile = new XmlCreatorFromFile();
        while (!XmlCreatorFromDb.finish){

        }
        xmlCreatorFromFile.getXMLFromFile("file_1.xml", "file_2.xml", "template.xlst");
        System.out.println(System.currentTimeMillis() - start);
    }
}

