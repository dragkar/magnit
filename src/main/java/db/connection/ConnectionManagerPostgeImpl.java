package db.connection;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Класс возвращает создает и возвращает соединение с базой данных
 */
public class ConnectionManagerPostgeImpl implements ConnectionManager {
    private static final Logger log = Logger.getLogger(ConnectionManagerPostgeImpl.class);
    private static ConnectionManager connectionManager;
    /**
     * default db params
     * @param urlDb url базы пример "jdbc:postgresql://localhost:5432/test"
     * @param nameDb логин для доступа к базе
     * @param passwordDb пароль
     * @param Driverdb доступные драйвера базы (POSTGRESQL, ORACLE, MYSQL)
     */
    public static final String PATH_TO_PROPERTIES = "src/main/resources/dbSettings.properties";

    private static String urlDb = "jdbc:postgresql://localhost:5432/magnit";
    private static String nameDb = "postgres";
    private static String passwordDb = "12345678";
    private static String DriverDb = "POSTGRESQL";

    /**
     * хранит в себе строки драйверов db
     */
    public final Map<String, String> urlDriverDB = new HashMap<String, String>() {{
        put("MYSQL", "jdbc:mysql://");
        put("ORACLE", "jdbc:oracle:thin:@");
        put("POSTGRESQL", "org.postgresql.Driver");

    }};

    public static ConnectionManager getInstance() {
        if (connectionManager == null) {
            connectionManager = new ConnectionManagerPostgeImpl();
        }
        return connectionManager;
    }


    private ConnectionManagerPostgeImpl() {
        setDbParams();
    }

    /**
     * @return возвращает соединение с базой
     */
    @Override
    public Connection getConnection() {
        Connection connection = null;
        try {
            if (urlDriverDB.containsKey(DriverDb)) {
                Class.forName(urlDriverDB.get(DriverDb));

                connection = DriverManager.getConnection(
                        urlDb,
                        nameDb,
                        passwordDb
                );
            } else {
                log.error("Такого драйвера нет");
                return null;
            }
        } catch (ClassNotFoundException e) {
            log.error("DB getDriver error");
            log.error(e.getStackTrace());
        } catch (SQLException e) {
            log.error("Connection with DB error");
            log.error(e.getStackTrace());
        }
        return connection;
    }

    /**
     * set db params from file dbSettings.properties
     */
    private static void setDbParams() {

        FileInputStream fileInputStream;
        Properties prop = new Properties();

        try {
            fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
            prop.load(fileInputStream);

            if (prop.getProperty("urlDb").length() > 0) {
                urlDb = prop.getProperty("urlDb");

            } else {
                log.error("urlDb in dbSettings.properties is empty");
                log.error("Program terminated");
                System.exit(0);
            }

            if (prop.getProperty("nameDb").length() > 0) {
                nameDb = prop.getProperty("nameDb");

            } else {
                log.error("nameDb in dbSettings.properties is empty");
                log.error("Program terminated");
                System.exit(0);
            }

            if (prop.getProperty("passwordDb").length() > 0) {
                passwordDb = prop.getProperty("passwordDb");

            } else {
                log.error("passwordDb in dbSettings.properties is empty");
                log.error("Program terminated");
                System.exit(0);
            }

            if (prop.getProperty("DriverDb").length() > 0) {
                DriverDb = prop.getProperty("DriverDb");
            } else {
                log.error("DriverDb in dbSettings.properties is empty");
                log.error("Program terminated");
                System.exit(0);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getUrlDb() {
        return urlDb;
    }

    public static void setUrlDb(String urlDb) {
        ConnectionManagerPostgeImpl.urlDb = urlDb;
    }

    public static String getNameDb() {
        return nameDb;
    }

    public static void setNameDb(String nameDb) {
        ConnectionManagerPostgeImpl.nameDb = nameDb;
    }

    public static String getPasswordDb() {
        return passwordDb;
    }

    public static void setPasswordDb(String passwordDb) {
        ConnectionManagerPostgeImpl.passwordDb = passwordDb;
    }

    public static String getDriverDb() {
        return DriverDb;
    }

    public static void setDriverDb(String driverDb) {
        DriverDb = driverDb;
    }
}
