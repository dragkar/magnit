package db.POJO;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Entity implements Serializable{
    private int field;

    public Entity() {
    }

    public Entity(int field) {
        this.field = field;
    }

    @XmlElement (name="field")
    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "field=" + field +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;
        Entity entity = (Entity) o;
        return Objects.equals(getField(), entity.getField());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getField());
    }


    @XmlRootElement
    public static class Entries {
        @XmlElement(name="entry")
        private List<Entity> entrys = new ArrayList<>();

        public List<Entity> getEntitys() {
            return entrys;
        }

        public void setEntry(List<Entity> entrys) {
            this.entrys = entrys;
        }

        public void add(Entity entry){
            entrys.add(entry);
        }
    }
}
