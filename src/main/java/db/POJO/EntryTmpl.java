package db.POJO;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * class for creat marshalling object from 2d file
 */
public class EntryTmpl {
    private int field;

    public EntryTmpl() {
    }

    public EntryTmpl(int field) {
        this.field = field;
    }

    @XmlAttribute
    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "field=" + field +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EntryTmpl)) return false;
        EntryTmpl entity = (EntryTmpl) o;
        return Objects.equals(getField(), entity.getField());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getField());
    }

    @XmlRootElement
    public static class Entries {
        @XmlElement(name="entry")
        private List<EntryTmpl> entrys = new ArrayList<>();

        public List<EntryTmpl> getEntitys() {
            return entrys;
        }

        public void setEntry(List<EntryTmpl> entrys) {
            this.entrys = entrys;
        }

        public void add(EntryTmpl entry){
            entrys.add(entry);
        }
    }
}
