package db.DAO;

import db.POJO.Entity;
import db.connection.ConnectionManager;
import db.connection.ConnectionManagerPostgeImpl;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EntityDaoImpl implements EntityDao {
    private static final Logger log = Logger.getLogger(ConnectionManagerPostgeImpl.class);
    private static ConnectionManager connectionManager =
            ConnectionManagerPostgeImpl.getInstance();

    public EntityDaoImpl() {
    }

    /**
     * Внесение записей в таблицу
     * @param from - начальный элемент для записи
     * @param to - конечный элемент для записи
     * @throws SQLException
     */
    @Override
    public void createAll(int from,int to) throws SQLException {

        try (Connection conn = connectionManager.getConnection()) {
            conn.setAutoCommit(false);
            String query = "INSERT INTO test " +
                    "(field) " +
                    "VALUES(?)";
            try (PreparedStatement preparedStmt = conn.prepareStatement(query)) {
                for (int i = from; i <= to; i++) {
                    preparedStmt.setInt(1, i);
                    preparedStmt.addBatch();
                }
                preparedStmt.executeBatch();
                conn.commit();
            } catch (SQLException e) {
                log.error(e.getStackTrace());
                throw e;
            }
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            log.error(e.getStackTrace());
            throw e;
        }
    }


    /**
     * Выбирает все поля
     * @return список всех полученных объектов
     * @throws SQLException
     */
    @Override
    public List<Entity> getAll() throws SQLException {
        Statement statement = null;
        List<Entity> listEntity = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * " +
                            "FROM  public.test "
            );
            while (resultSet.next()) {
                Entity entity = new Entity(
                        resultSet.getInt("field")
                );
                listEntity.add(entity);
            }
        } catch (SQLException e) {
            log.error(e.getStackTrace());
            throw e;
        } finally {
            statement.close();
        }
        return listEntity;
    }

    /**
     * удаление всех полей из таблицы test
     */
    @Override
    public void deleteAllFromTest() throws SQLException {
        PreparedStatement preparedStmt = null;
        try (Connection connection = connectionManager.getConnection()) {
            String query = "DELETE FROM public.test ";
            preparedStmt = connection.prepareStatement(query);
            preparedStmt.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getStackTrace());
            throw e;
        }

    }

}
