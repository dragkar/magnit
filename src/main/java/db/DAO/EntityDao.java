package db.DAO;

import db.POJO.Entity;

import java.sql.SQLException;
import java.util.List;

public interface EntityDao {
    void createAll(int from,int to) throws SQLException;
    List<Entity> getAll() throws SQLException;
    public void deleteAllFromTest() throws SQLException;
}
