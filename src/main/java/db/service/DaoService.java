package db.service;

import db.DAO.EntityDao;
import db.DAO.EntityDaoImpl;

import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DaoService {
    private static final Logger log = Logger.getLogger(DaoService.class);


    final int POOL_SIZE = 15; /** размер пула потоков*/
    EntityDao entityDao = new EntityDaoImpl();


    /**
     * inserting  elements to db
     *
     * @param n - number of elements
     */
    public void inertElements(int n) {
        try {
            entityDao.deleteAllFromTest();
            log.info("Delete records is complete");
        } catch (SQLException e) {
            log.error("Error delete record from db: ");
        }

        int from = 0;
        int to = 0;
        ExecutorService pool = Executors.newFixedThreadPool(POOL_SIZE);
        int step = 0;
        if (n > 0) {
            step = n / POOL_SIZE;
            to = step;
        }

        List<Inserter> inserterList = new ArrayList<>();
        for (int i = 0; i <= POOL_SIZE; i++) {
            Inserter inserter = new Inserter(from, to);
            from = to+1;
            to += step;
            if (i == (POOL_SIZE-1) ) {
                to = n;
            }
            inserterList.add(inserter);
        }
        try {
            pool.invokeAll(inserterList);
        } catch (InterruptedException e) {
            log.error("Ошибка при работе с потоками добавления записей" + e.getStackTrace());
        }
        pool.shutdown();

        log.info("Creating records is complete ");
    }

    /**
     *
     * class for insert in multithreads
     */
    public class Inserter implements Callable<Integer> {
        private int from;
        private int to;

        public Inserter(int from, int to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public Integer call() {
            try {
                entityDao.createAll(from, to);
            } catch (SQLException e) {
                log.error("Ошибка в блоке добавления записей" + from + "," + to + "]");
            }
            return 1;
        }
    }

}
