package db.service;

import db.DAO.EntityDao;
import db.DAO.EntityDaoImpl;
import db.POJO.Entity;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.sql.SQLException;
import java.util.List;

public class XmlCreatorFromDb {
    private static final Logger log = Logger.getLogger(XmlCreatorFromDb.class);
public static boolean finish = false;
    /**
     * create first file
     *
     * @param path
     */
    public void createXmlFile(String path) {
        File file = new File(path);
        finish = false;
        EntityDao entityDao = new EntityDaoImpl();
        JAXBContext context;
        Marshaller marshaller = null;
        try {
            context = JAXBContext.newInstance(Entity.Entries.class);
            marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        } catch (JAXBException e) {
            log.error("error JAXBException : ");
        }

        Entity.Entries entries = new Entity.Entries();

        try {
            List<Entity> entities= entityDao.getAll();
            for (Entity entity : entities) {
                entries.add(entity);
            }
        } catch (SQLException e) {
            log.error("Error read from database");
        }
        try {
            marshaller.marshal(entries, file);
            log.info("creation of the 1st file is complete");
            finish = true;
        } catch (JAXBException e) {
            log.error("Error marshalling: ");
        }
    }
}
