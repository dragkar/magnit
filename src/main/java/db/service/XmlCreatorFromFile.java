package db.service;

import db.POJO.EntryTmpl;

import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;


public class XmlCreatorFromFile {
    private static final Logger log = Logger.getLogger(XmlCreatorFromFile.class);

    /**
     * parce xml file
     * @param filePath
     */
    private static void fromXmlToObject(String filePath) {
        try {
            long sum = 0;
            // создаем объект JAXBContext - точку входа для JAXB
            JAXBContext jaxbContext = JAXBContext.newInstance(EntryTmpl.Entries.class);
            Unmarshaller un = jaxbContext.createUnmarshaller();

            EntryTmpl.Entries entries = (EntryTmpl.Entries) un.unmarshal(new File(filePath));
            for (EntryTmpl entity : entries.getEntitys()) {
                sum += entity.getField();
            }
            log.info("Сумма : "+ sum);

        } catch (JAXBException e) {
            log.error("Error : ");
            log.error(e.getStackTrace());
        }

    }


    /**
     * transform file
     *
     * @param readFileName -
     * @param writeFileName
     * @param templateFile
     */
    public  void getXMLFromFile(String readFileName, String writeFileName, String templateFile) {
        try {
            TransformerFactory tf =
                    TransformerFactory.newInstance();
//установка используемого XSL-преобразования
            Transformer transformer =
                    tf.newTransformer(new StreamSource(templateFile));
//установка исходного XML-документа и конечного XML-файла
            transformer.transform(
                    new StreamSource(readFileName),
                    new StreamResult(writeFileName));
            log.info("creation of the second file is complete");
        } catch(TransformerException e) {
            log.error("Can not transform to second xml file");
            log.error(e.getStackTrace());
        }
        fromXmlToObject(writeFileName);
    }

}
